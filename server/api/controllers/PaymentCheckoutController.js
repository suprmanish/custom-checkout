/**
 * PaymentCheckoutController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  async startPaymentCheckout(req, res) {
    const data = req.query;
    res.view("pages/razorpayCheckout.ejs", { data });
  },

  async razorpayCallbackHandler(req, res) {
    // eslint-disable-next-line no-console
    const { razorpay_payment_id, razorpay_order_id } = req.body;
    let success = true;
    if (!razorpay_order_id || razorpay_order_id) {
      success = false;
    }
    res.view("pages/paymentCallback.ejs", {
      success,
      razorpay_payment_id,
      razorpay_order_id,
    });
  },
};
