import { Component } from "@angular/core";

import {
  InAppBrowser,
  InAppBrowserOptions,
} from "@ionic-native/in-app-browser/ngx";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  constructor(private inAppBrowser: InAppBrowser) {}

  handleButtonClick() {
    const razorpayData = {
      amount: 1000,
      currency: "INR",
      contact: "9742920606",
      email: "riteshjena1192@gmail.com",
      paymentInfo: JSON.stringify({
        method: "netbanking",
        bank: "UTIB",
      }),
    };

    const query = Object.entries(razorpayData)
      .map(([k, val]) => `${k}=${val}`)
      .join("&");

    const options: InAppBrowserOptions = {
      zoom: "no",
      clearcache: "yes",
      cleardata: "yes",
      clearsessioncache: "yes",
      location: "no",
    };

    const browser = this.inAppBrowser.create(
      `http://10.0.2.2:1337/paymentCheckout?${query}`,
      "_blank",
      options
    );

    browser.on("loadstop").subscribe((event) => {
      const { url } = event;
      if (url.indexOf("postPayment") > -1) {
        alert("Payment Completed on Razorpay");
        browser.close();
      }
    });
  }
}
