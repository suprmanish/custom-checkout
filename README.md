# README

### What is this repository for?

Demo ionic cordova + razorpay custom checkout integration

### How do I get set up?

Clone repo
npm install -g @ionic/cli cordova sails
cd into app folder then npm install
cd into server folder then npm install

#### Start server:

cd into server folder. Then "np run start" or "sails lift"
Restart server if any changes are made

#### Start app on emulator:

cd into app folder
ionic cordova run android --emulator --livereload
